from itertools import chain
from statistics import mean
import sys
import os
from typing import List
from kimiworker import (
    Worker,
    KimiLogger,
    WorkerJob,
    ContentDefinition,
    PublishMethod,
    get_enclosing_bbox,
    find_consecutive_element_indices_with_label,
    merge_labels,
    has_label,
)

from minio import Minio
from pathdict import PathDict
from shapely.geometry import Polygon
from shapely import STRtree
from labels import (
    apply_region_labels,
)

numberOfConcurrentJobs = int(os.getenv("CONCURRENT_JOBS", "1"))


def get_interpreted_content(job: WorkerJob) -> List[ContentDefinition] | None:
    """Gets interpreted content from worker job"""
    attributes = PathDict(job.get("attributes"))

    best_job_id = attributes["interpretedContent.bestJobId"]
    if best_job_id is None:
        return

    values = attributes["interpretedContent.values"]
    best_value = next((x for x in values if x["jobId"] == best_job_id), None)
    if best_value is None:
        return

    return best_value["contentResult"]


def get_regions(job: WorkerJob) -> List[ContentDefinition]:
    """Gets regions from worker job"""
    attributes = PathDict(job.get("attributes"))
    values = attributes["regions.values"]

    if values is None:
        return []

    # get contentResult from each value
    values = [value["contentResult"] for value in values]

    # Flatten the array and return it
    return list(chain.from_iterable(values))


def job_handler(
    log: KimiLogger,
    job: WorkerJob,
    job_id: str,
    local_file_path: str,
    minio: Minio,
    publish: PublishMethod,
):
    elements = get_interpreted_content(job)

    if elements is None:
        publish("error", "interpreted content not found")
        return

    regions = get_regions(job)

    print([region.get("bbox") for region in regions if has_label(region, "table")])
    regions_tree = STRtree(
        [
            Polygon(
                [
                    [region.get("bbox").get("left"), region.get("bbox").get("top")],
                    [region.get("bbox").get("right"), region.get("bbox").get("top")],
                    [region.get("bbox").get("right"), region.get("bbox").get("bottom")],
                    [region.get("bbox").get("left"), region.get("bbox").get("bottom")],
                    [region.get("bbox").get("left"), region.get("bbox").get("top")],
                ]
            )
            for region in regions
        ]
    )

    # Sort the elements
    elements.sort(key=lambda element: element.get("bbox").get("top"))

    # Remove elements with a confidence level that is too low
    elements = list(filter(lambda element: element.get("confidence") > 50, elements))

    # Apply labels to elements
    for i, element in enumerate(elements):
        # Apply labels to the elements
        apply_region_labels(element, regions, regions_tree)

    # Remove elements with a table label
    elements = list(filter(lambda element: not has_label(element, "table"), elements))

    # Look for elements that have label has_neighbor_top
    elements_with_top_neighbors = find_consecutive_element_indices_with_label(
        elements,
        ["has_neighbor_top"],
        ["table", "section_header", "title", "picture", "page_footer"],
    )

    # Iterate over groups
    for consecutive_element_indices in elements_with_top_neighbors:
        start = max(consecutive_element_indices[0] - 1, 0)
        end = consecutive_element_indices[-1] + 1
        consecutive_elements = (
            [elements[1]] if start == 1 and end == 1 else elements[start:end]
        )

        first_element_text = str(elements[start].get("attributes").get("text") or "")
        second_element_text = str(
            elements[start + 1].get("attributes").get("text") or ""
        )

        # Don't include the first element if the length of the line is way shorter than the second
        if len(first_element_text) / len(second_element_text) < 0.4:
            start += 1

        labels = merge_labels(
            [], [element.get("labels") or [] for element in consecutive_elements]
        )

        paragraph: ContentDefinition = {
            "classification": "application/x-nldoc.element.text+paragraph",
            "labels": labels,
            "confidence": mean(
                [element.get("confidence") for element in consecutive_elements]
            ),
            "bbox": get_enclosing_bbox(consecutive_elements),
            "attributes": {
                "text": " ".join(
                    filter(
                        None,
                        [
                            str(element.get("attributes").get("text") or "")
                            for element in consecutive_elements
                        ],
                    )
                ),
            },
            "children": consecutive_elements.copy(),
        }

        for element in consecutive_elements:
            element.get("attributes").update({"remove": "true"})

        elements.append(paragraph)

    elements = list(
        filter(
            lambda element: element.get("attributes").get("remove", "false") == "false",
            elements,
        )
    )

    log.info("Job done")

    page_content: List[ContentDefinition] = [
        {
            "classification": "application/x-nldoc.page",
            "labels": [],
            "confidence": 100,
            "bbox": {"top": 0, "bottom": 1, "left": 0, "right": 1},
            "attributes": {},
            "children": elements,
        }
    ]

    publish(
        "contentResult",
        page_content,
        success=True,
        confidence=100,
    )


if __name__ == "__main__":
    try:
        worker = Worker(job_handler, "worker-page-content", True)
        worker.start(numberOfConcurrentJobs)

    except KeyboardInterrupt:
        print("\n")
        print("Got interruption signal. Exiting...\n")
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)

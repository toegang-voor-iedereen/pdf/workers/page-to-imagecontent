from shapely import STRtree
from shapely.geometry import Polygon

from kimiworker import ContentDefinition, merge_labels


def apply_region_labels(
    element: ContentDefinition, regions: list[ContentDefinition], regions_tree: STRtree
):
    element_bbox = element.get("bbox")
    element_polygon = Polygon(
        [
            [element_bbox.get("left"), element_bbox.get("top")],
            [element_bbox.get("right"), element_bbox.get("top")],
            [element_bbox.get("right"), element_bbox.get("bottom")],
            [element_bbox.get("left"), element_bbox.get("bottom")],
            [element_bbox.get("left"), element_bbox.get("top")],
        ]
    )
    region_indices: list[int] = regions_tree.query(element_polygon).tolist()
    region_labels = [regions[i].get("labels") or [] for i in region_indices]
    labels = element.get("labels") or []
    merged_labels = merge_labels(labels, region_labels)
    element.update(({"labels": merged_labels}))
    for child in element.get("children") or []:
        apply_region_labels(child, regions, regions_tree)

"""Tests for label helpers"""

from shapely import STRtree
from shapely.geometry import Polygon
from kimiworker import ContentDefinition

from labels import apply_region_labels


element: ContentDefinition = {
    "classification": "some-classification",
    "confidence": 100,
    "attributes": {},
    "bbox": {"top": 0.1, "left": 0.1, "right": 0.3, "bottom": 0.3},
    "labels": [{"name": "existing", "confidence": 95}],
    "children": [],
}

region1: ContentDefinition = {
    "classification": "some-classification",
    "confidence": 100,
    "attributes": {},
    "bbox": {"top": 0.0, "left": 0.0, "right": 0.2, "bottom": 0.2},
    "labels": [{"name": "region1", "confidence": 90}],
    "children": [],
}

region2: ContentDefinition = {
    "classification": "some-classification",
    "confidence": 100,
    "attributes": {},
    "bbox": {"top": 0.2, "left": 0.2, "right": 0.4, "bottom": 0.4},
    "labels": [{"name": "region2", "confidence": 85}],
    "children": [],
}

regions = [region1, region2]
polygons = [
    Polygon(
        [
            [region.get("bbox").get("left"), region.get("bbox").get("top")],
            [region.get("bbox").get("right"), region.get("bbox").get("top")],
            [region.get("bbox").get("right"), region.get("bbox").get("bottom")],
            [region.get("bbox").get("left"), region.get("bbox").get("bottom")],
            [region.get("bbox").get("left"), region.get("bbox").get("top")],
        ]
    )
    for region in regions
]
regions_tree = STRtree(polygons)


def test_apply_region_labels_intersection():
    apply_region_labels(element, regions, regions_tree)
    expected_labels = [
        {"name": "existing", "confidence": 95},
        {"name": "region1", "confidence": 90},
        {"name": "region2", "confidence": 85},
    ]
    assert (
        element["labels"] == expected_labels
    ), "Labels were not merged correctly for intersecting regions"


def test_apply_region_labels_no_intersection():
    element_no_intersect: ContentDefinition = {
        "classification": "some-classification",
        "confidence": 100,
        "attributes": {},
        "bbox": {"top": 0.5, "left": 0.5, "right": 0.7, "bottom": 0.7},
        "labels": [{"name": "isolated", "confidence": 99}],
        "children": [],
    }
    apply_region_labels(element_no_intersect, regions, regions_tree)
    expected_labels = [{"name": "isolated", "confidence": 99}]
    assert (
        element_no_intersect["labels"] == expected_labels
    ), "Labels changed incorrectly for non-intersecting regions"
